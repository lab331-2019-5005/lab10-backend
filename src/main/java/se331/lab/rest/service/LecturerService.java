package se331.lab.rest.service;

import se331.lab.rest.entity.Lecturer;

import java.util.List;

public interface LecturerService {
    List<Lecturer> getAllLecturer();
    Lecturer findById(Long lecturerId);
    Lecturer saveLecturer(Lecturer lecturer);
}

