package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.entity.Course;

import java.util.List;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService {
    @Autowired
    CourseDao courseDao;

    @Override
    public List<Course> getAllCourse() {
        log.info("service received called");
        List<Course> courses = courseDao.getAllCourse();
        log.info("service received  from dao");
        return courses;
    }

    @Override
    public Course findById(Long courseId) {
        return courseDao.findById(courseId);
    }

    @Override
    public Course saveCourse(Course course) {
        return courseDao.saveCourse(course);
    }


}

