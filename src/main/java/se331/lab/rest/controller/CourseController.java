package se331.lab.rest.controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Course;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseService;

@Controller
@Slf4j
public class CourseController {

    @Autowired
    CourseService courseService;

    @GetMapping("/courses")
    public ResponseEntity getAllCourse() {
        log.info("the controller is call");
        //return ResponseEntity.ok(courseService.getAllCourse());
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courseService.getAllCourse()));
    }


    @GetMapping("/courses/{id}")
    public ResponseEntity getCourseById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(courseService.findById(id));
    }

    @PostMapping("/courses")
    public ResponseEntity saveLecturer(@RequestBody Course course) {
        return ResponseEntity.ok(courseService.saveCourse(course));
    }

}

