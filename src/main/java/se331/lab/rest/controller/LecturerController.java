package se331.lab.rest.controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.LecturerService;

@Controller
@Slf4j
public class LecturerController {

    @Autowired
    LecturerService lecturerService;

    @GetMapping("/lecturers")
    public ResponseEntity getAllLecturer() {
        log.info("the controller is call");
        //return ResponseEntity.ok(lecturerService.getAllLecturer());
        return ResponseEntity.ok(MapperUtil.INSTANCE.getLecturerDto(lecturerService.getAllLecturer()));
    }

    @GetMapping("/lecturers/{id}")
    public ResponseEntity getLecturerById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(lecturerService.findById(id));
    }

    @PostMapping("/lecturers")
    public ResponseEntity saveLecturer(@RequestBody Lecturer lecturer) {
        return ResponseEntity.ok(lecturerService.saveLecturer(lecturer));
    }
}
