package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    Integer penAmount;
    String description;

    @ManyToOne
    @JsonBackReference
            Lecturer advisor;
    @ManyToMany(mappedBy = "students")

    @JsonManagedReference
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Builder.Default
    List<Course> enrolledCourses = new ArrayList<>();


}
