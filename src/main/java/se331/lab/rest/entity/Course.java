package se331.lab.rest.entity;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String content;
    String courseId;
    String courseName;

    @ManyToOne
    @JsonBackReference
    Lecturer lecturer;
    @ManyToMany
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Student> students = new ArrayList<>();
}
