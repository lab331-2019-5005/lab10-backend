package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.repository.LecturerRepository;

import java.util.List;
@Repository
@Profile("lecturerDbDao")
@Slf4j
public class LecturerDBDaoImpl implements LecturerDao {
    @Autowired
    LecturerRepository lecturerRepository;

    @Override
    public List<Lecturer> getAllLecturer() {
        log.info("find all lecturer in db");
        return lecturerRepository.findAll();
    }

    @Override
    public Lecturer findById(Long lecturerId) {
        log.info("find student from id {} from database", lecturerId);
        return lecturerRepository.findById(lecturerId).orElse(null);
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        log.info("save lecturer to database");
        return lecturerRepository.save(lecturer);
    }
}
