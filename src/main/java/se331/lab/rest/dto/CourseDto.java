package se331.lab.rest.dto;

import lombok.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CourseDto {
    Long id;
    String courseId;
    String courseName;
    List<StudentDto> students = new ArrayList<>();
//    LecturerDto lecturer;
}
